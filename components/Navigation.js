import React from 'react';
import { NavLink } from 'react-router-dom';

const Navigation = () => {
    return (
        <div className="navigation" >
            <NavLink exact to="" activeClassename="nav-active">
                Accueil
            </NavLink>
            <NavLink exact to="about" activeClassename="nav-active">
                about
            </NavLink>
        </div>
    );
};

export default Navigation;