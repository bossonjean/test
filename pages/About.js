import React from 'react';
import Logo from '../components/Logo';
import Navigation from '../components/Navigation';

const About = () => {
    return (
        <div className="about">
        <Navigation/>
        <Logo/>
            <h1>A propos</h1>
            <br/>
            <p>
                lorem,Un texte est une série orale ou écrite de mots perçus comme constituant 
                un ensemble cohérent, porteur de sens et utilisant les structures propres à une langue. 
                Un texte n'a pas de longueur 
                déterminée sauf dans le cas de poèmes à forme fixe comme le sonnet ou le haïku. 
               
            </p>
            <br/>
            <p>
                        utilisez un vocabulaire qu'il comprend ;
            construisez des phrases courtes et logiques ;
            regroupez vos idées en paragraphes cohérents et reliez-les par des connecteurs logiques ;
            soyez concis : éliminez tout mot ou information inutiles ;. 
               
            </p>

        </div>
    );
};

export default About;